"""
A tool to write the totp.txt file so you do not have to manually enter all data on the watch. File format is:

salt:key:checksum
name:secret
name:secret
~~~~~~~~~~~
etc

where:
salt = some characters that are concatenated to password before hashing
key = the key used to encrypt the other values, encrypted with salted hash of password
checksum = always 'wasp' encrypted with the key, used to verify correct password
name = the name of an entry
secret = the preshared secret of the entry
"""

import hashlib
import secrets
if input("""WARNING:

To use this tool, you must either type or copy and paste TOTP secrets in plain
text. You will also have to type a password to protect your secrets. Make sure
you trust this program, the operating system, and all software that may have
access to the secrets. Make sure you do not have any keyloggers, clipboard
loggers, or other malware before proceeding. If possible, it is best to do this
on a fresh live USB that you never connect to the internet running on bare
metal. When you are done, it is best to wipe that live USB. The resulting file
should theoretically be safe to put anywhere, as long as the password remains
secret.

To continue, type 'yes' in all capitals.""") != 'YES':
    raise SystemExit(0)

import sys

file = open(sys.argv[1] if len(sys.argv >= 2) else 'totp.txt')

userPassword = input("""Enter the password. Note that it will be converted into a shortened simplified T9 string to make it easier to enter. This means that:

Numbers get left as they are
'.' , ',', and '!' become 1
a, b, and c become 2
d, e, and f become 3
g, h, and i become 4
j, k, and l become 5
m, n, and o become 6
p, q, r, and s become 7
t, u, and v become 8
w, x, y, and z become 9
Spaces and all other symbols become 0

Password: """)

password = ''
for character in userPassword:
    if character in '.,!1':
        password.append('1')
    elif character in 'abc2':
        password.append('2')
    elif character in 'def3':
        password.append('3')
    elif character in 'ghi4':
        password.append('4')
    elif character in 'jkl5':
        password.append('5')
    elif character in 'mno6':
        password.append('6')
    elif character in 'pqrs7':
        password.append('7')
    elif character in 'tuv8':
        password.append('8')
    elif character in 'wxyz9':
        password.append('9')
    else:
        password.append('0')

print(
    f'Your password is {password}, but it is OK if you only remember {userPassword}')

# Salt and hash password to make key for encryption key
salt = secrets.token_bytes(32)
passwordKey = password.encode() + salt
for _ in range(256):    # Do many rounds of hashing to make brute forcing more difficult. I would do more, but we're limited by the computing power of the watch
    passwordKey = hashlib.sha256(password)
# Generate real key and salt key
key = secrets.token_bytes(32)
saltKey = hashlib.sha256(passord.encode()).digest()
# TODO Write information to file
file.write(f'::')
while True:
    name = input("Secret name: ")
    secret = input("Secret: ")
    # TODO Encrypt name and secret with key and write to file
    if input('Enter another service').lower()[0] == 'n':
        break
