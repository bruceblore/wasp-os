Watch Application System in Python (Bruce Blore's version)
==========================================================

Introduction
------------

This repository features my modifications to Wasp OS, made primarily for my own
use with my PineTime, but published here in the spirit of open source. Please
also check out the `upstream project <https://github.com/daniel-thompson/wasp-os>`_.

Wasp-os is a firmware for smart watches that are based on the nRF52 family of
microcontrollers, and especially for hacker friendly watches such as the Pine64
PineTime. Wasp-os features full heart rate monitoring and step counting support
together with multiple clock faces, a stopwatch, an alarm clock, a countdown
timer, a calculator and lots of other games and utilities. All of this, and
still with access to the MicroPython REPL for interactive tweaking, development
and testing.

Wasp-os comes fully integrated with a robust bootloader based on the Adafruit
NRF52 Bootloader. The bootloader has been extended to make it robust for
development on form-factor devices without a reset button, power switch, SWD
debugger or UART. This allows us to confidently develop on sealed devices
relying on Bluetooth Low Energy for over-the-air updates.

My modifications
----------------

Already done:

 * Adjusted the quick ring to suit my needs
    * Added timer
    * Rearranged so stopwatch and timer on right, health on left
 * Removed software that I don't use from manifest
    * Analog face
    * Dual face
    * Face selector
    * Fibonacci face
    * Game of life
    * Haiku
    * Music player
    * Word clock face
    * Weather
    * Gadgetbridge integration
 * Renamed Torch to Light to better suit conventions in my country
 * Made alarm, calculator, light, pomodoro, rng, sports, and free memory enabled by default to be better compliant with doing all RAM allocation at once to reduce fragmentation
 * Changed default brightness and notification strength to reflect personal preferences
 * Calculator
    * comes back if watch goes to sleep
    * Reverse polish notation
    * No eval so shouldn't be affected by memory problems
 * Improvements to watch face
    * same color for every digit (personal preference)
    * day of week
    * seconds
    * text on watch face when swiping down
 * Alarm
    * Made vibrations more powerful
    * Made button silence the alarm (harder to accidentally press, requires more awakeness)
    * Made swiping snooze the alarm (still doable but requires doing intentionally, but easier than pressing the button)
    * Set snooze time to 5 minutes
    * Tapping neither silences nor snoozes (too easy to do accidentally)
    * Now has one 6:30 weekday alarm active by default
 * Weather app
    * no longer keeps watch awake
    * now works kinda ok with gadgetbridge
    * removed due to memory fragmenatation with Gadgetbridge
 * Enabled the garbage collector
 * Invert display option in settings
 * Stopwatch
    * Vibration feedback
    * Turn off screen but don't go to sleep
 * Unregistered apps are deleted from RAM using `del`, not just hidden in UI
 * Random number generator
    * Dice roller
    * coin flipper
    * card drawer
 * Timer
    * Support keypad rather than arrows to enter numbers
    * Keep awake when running
    * Now must tap near stop button, not just somewhere on screen
 * 2048
    * Keeps watch awake
    * No longer crashes when progressing too far
 * Snake improvements
    * Doesn't end when hitting edges (small screen forgiveness)
    * Taps are now supported in addition to swipes
    * Grid showing where to tap
 * Replaced test app with lighter memory checker
 * Search for and fix 'import icons' to save a bit of memory
 * Pomodoro timer
 * Icon for sports app
 * Automatic brightness by time of day
 * Tap to wake

To do (listed in approximate order of how much I want these features, and
therefore approximate order of what I'll work on):
 * Timer
    * Actually render colins in time editor
    * Get rid of blank space and expand keys
    * Display actual hours in timer screen
 * Stopwatch enhancements:
    * Lap logging to file
 * Random number generator
    * multiple dice, larger font
    * optional jokers
    * generic customizeable RNG

Modifications I'd want to make, but are currently outside of my abilities:
 * TOTP authenticator: Memory constraints
 * Make ticks align with seconds on watch face: Can't figure out how to get current time more precise than 1s to be used for setting first tick period
 * Bluetooth settings: Can't find where Bluetooth is implemented and configured
    * on/off to save power and for flights
    * enable/disable software writing/remote code execution, so malware can't be uploaded
    * Authentication and MAC address restriction so random people cannot mess with my watch remotely

Documentation
-------------

Wasp-os is has `extensive documentation <https://wasp-os.readthedocs.io>`_
which includes a detailed `Application Writer's Guide
<https://wasp-os.readthedocs.io/en/latest/appguide.html>`_ to help you
get started coding for wasp-os as quickly as possible.

Getting Started
---------------

Wasp-os can be installed without using any tools or disassembly onto the
following devices:

 * Pine64 PineTime
 * Colmi P8
 * Senbono K9

Use the
`Installation Guide <https://wasp-os.readthedocs.io/en/latest/install.html>`_
to learn how to build and install wasp-os on these devices.

At the end of the install process your watch will show the time (03:00)
together with a date and a battery meter. When the watch goes into power
saving mode you can use the button to wake it again.

At this point you will also be able to use the Nordic UART Service to
access the MicroPython REPL. You can use ``tools/wasptool --console``
to access the MicroPython REPL.

To set the time and restart the main application:

.. code-block:: python

   ^C
   watch.rtc.set_localtime((yyyy, mm, dd, HH, MM, SS))
   wasp.system.run()

Or, if you have a suitable GNU/Linux workstation, just use:

.. code-block:: sh

   ./tools/wasptool --rtc

which can run these commands automatically.

As mentioned above there are many drivers and features still to be
developed, see the :ref:`Roadmap` for current status.

Community
---------

The wasp-os community is centred around the
`github project <https://github.com/daniel-thompson/wasp-os>`_ and is
supplemented with instant messaging via the #wasp-os IRC channel at
libera.chat .

If you are unfamiliar with IRC and don't have a preferred client then
we recommend connecting to libera.chat using the
`matrix/IRC bridge <https://app.element.io/#/room/#wasp-os:libera.chat>`_.
The matrix bridge will allow us to receive messages whilst offline. Follow
the link above and, if you do not already have a matrix account, register
yourself. That should be enough to get you chatting!

Videos
------

.. list-table::

   * - .. figure:: res/thumbnail-nps8Kd2qPzs.jpg
          :target: https://www.youtube.com/watch?v=nps8Kd2qPzs
          :alt: wasp-os: A tour of the new applications for wasp-os
          :width: 95%

          `A tour of the new applications for wasp-os <https://www.youtube.com/watch?v=nps8Kd2qPzs>`_

     - .. figure:: https://img.youtube.com/vi/lIo2-djNR48/0.jpg
          :target: https://www.youtube.com/watch?v=lIo2-djNR48
          :alt: wasp-os: Open source heart rate monitoring for Pine64 PineTime
          :width: 95%

          `Open source heart rate monitoring for Pine64 PineTime <https://www.youtube.com/watch?v=lIo2-djNR48>`_

   * - .. figure:: https://img.youtube.com/vi/YktiGUSRJB4/0.jpg
          :target: https://www.youtube.com/watch?v=YktiGUSRJB4
          :alt: An M2 pre-release running on Pine64 PineTime
          :width: 95%

          `An M2 pre-release running on Pine64 PineTime <https://www.youtube.com/watch?v=YktiGUSRJB4>`_

     - .. figure:: https://img.youtube.com/vi/tuk9Nmr3Jo8/0.jpg
          :target: https://www.youtube.com/watch?v=tuk9Nmr3Jo8
          :alt: How to develop wasp-os python applications on a Pine64 PineTime
          :width: 95%

          `How to develop wasp-os python applications on a Pine64 PineTime <https://www.youtube.com/watch?v=tuk9Nmr3Jo8>`_

   * - .. figure:: https://img.youtube.com/vi/kf1VHj587Mc/0.jpg
          :target: https://www.youtube.com/watch?v=kf1VHj587Mc
          :alt: Developing for Pine64 PineTime using wasp-os and MicroPython
          :width: 95%

          `Developing for Pine64 PineTime using wasp-os and MicroPython <https://www.youtube.com/watch?v=kf1VHj587Mc>`_

     -

Screenshots
-----------

(An older version of) the digital clock application running on a Pine64
PineTime:

.. image:: res/clock_app.jpg
   :alt: wasp-os digital clock app running on PineTime
   :width: 233

Screenshots of the built in applications running on the wasp-os
simulator:

.. image:: res/Bootloader.png
   :alt: Bootloader splash screen overlaid on the simulator watch art
   :width: 179

.. image:: res/ClockApp.png
   :alt: Digital clock application running on the wasp-os simulator
   :width: 179

.. image:: res/DemoApp.png
   :alt: Simple always-on demo for showing off wasp-os at conferences and shows
   :width: 179

.. image:: res/HeartApp.png
   :alt: Heart rate application running on the wasp-os simulator
   :width: 179

.. image:: res/SportsApp.png
   :alt: Sports applications, a combined stopwatch and step counter
   :width: 179

.. image:: res/StopclockApp.png
   :alt: Stop watch application running on the wasp-os simulator
   :width: 179

.. image:: res/StepsApp.png
   :alt: Step counter application running on the wasp-os simulator
   :width: 179

.. image:: res/LauncherApp.png
   :alt: Application launcher running on the wasp-os simulator
   :width: 179

.. image:: res/SettingsApp.png
   :alt: Settings application running on the wasp-os simulator
   :width: 179

.. image:: res/SoftwareApp.png
   :alt: Software selection app running on the wasp-os simulator
   :width: 179


wasp-os also contains a library of additional applications for you to choose.
These are disabled by default but can be easily enabled using the Software
application (and the "blank" white screen is a torch application):

.. image:: res/SelfTestApp.png
   :alt: Self test application running a rendering benchmark on the simulator
   :width: 179

.. image:: res/TorchApp.png
   :alt: Torch application running on the wasp-os simulator
   :width: 179

.. image:: res/ChronoApp.png
   :alt: Analogue clock application running in the wasp-os simulator
   :width: 179

.. image:: res/DualApp.png
   :alt: An other clock application running in the wasp-os simulator
   :width: 179

.. image:: res/FiboApp.png
   :alt: Fibonacci clock application running in the wasp-os simulator
   :width: 179

.. image:: res/HaikuApp.png
   :alt: Haiku application running in the wasp-os simulator
   :width: 179

.. image:: res/LifeApp.png
   :alt: Game of Life running in the wasp-os simulator
   :width: 179

.. image:: res/AlarmApp.png
   :alt: Alarm clock application running in the wasp-os simulator
   :width: 179

.. image:: res/MusicApp.png
   :alt: Music Player running in the wasp-os simulator
   :width: 179

.. image:: res/CalcApp.png
   :alt: Calculator running in the wasp-os simulator
   :width: 179

.. image:: res/2048App.png
   :alt: Let's play the 2048 game (in the wasp-os simulator)
   :width: 179

.. image:: res/SnakeApp.png
   :alt: Snake Game running in the wasp-os simulator
   :width: 179

.. image:: res/TimerApp.png
   :alt: Countdown timer application running in the wasp-os simulator
   :width: 179

.. image:: res/WeatherApp.png
   :alt: Weather application running in the wasp-os simulator
   :width: 179

.. image:: res/WordClkApp.png
   :alt: Shows a time as words in the wasp-os simulator
   :width: 179
