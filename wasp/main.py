# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson

# wasp-os setup
import wasp
# from gadgetbridge import *
wasp.gc.enable()  # Why isn't this done by default?
wasp.system.schedule()
