# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson
"""Shared manifest for applications that work well on a 240x240 display."""

# Feel free to enable anything I have commented out, but remember that you will also have to enable them in
# apps/software.py (to add checkboxes to enable) or in wasp.py (register_defaults function) (to enable by default).
# If it is not specified why it is disabled, assume that there is no problem with it and I simply do not use it.
# If there are problems or other dependencies, they will be noted.
manifest = (
    'apps/alarm.py',
    'apps/calc.py',
    'apps/clock.py',
    # 'apps/chrono.py',
    # 'apps/dual_clock.lpy',        # Must also enable fonts/clock_dual.py near bottom
    # 'apps/faces.py',
    # 'apps/fibonacci_clock.py',
    'apps/flashlight.py',
    'apps/freemem.py',
    # 'apps/gameoflife.py',
    # 'apps/haiku.py',              # Excessive memory usage and fragmentation
    'apps/heart.py',
    # 'apps/musicplayer.py',        # Tends to freeze the watch, requiring a reboot to fix
    'apps/launcher.py',
    'apps/pager.py',
    'apps/play2048.py',
    'apps/pomodoro.py',
    'apps/rng.py',
    'apps/settings.py',
    'apps/software.py',
    'apps/sports.py',
    'apps/steps.py',
    'apps/stopwatch.py',
    'apps/snake.py',
    'apps/timer.py',
    # 'apps/totp.py',               # Enabled in simulator only, not nearly ready for real hardware yet
    # 'apps/weather.py',            # Gadgetbridge causes severe memory fragmentation
    # 'apps/word_clock.py',
    'fonts/__init__.py',
    'fonts/clock.py',
    # 'fonts/clock_dual.py',        # Must enable if using apps/dual_clock, should not enable if not using apps/dual_clock
    'fonts/sans18.py',
    'fonts/sans24.py',
    'fonts/sans28.py',
    'fonts/sans36.py',
    'icons.py',
    'steplogger.py',
    'widgets.py'
)
