# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson

"""Haiku viewer
~~~~~~~~~~~~~~~

These three lines poems are fun to write and fit nicely on a tiny screen.

.. figure:: res/HaikuApp.png
    :width: 179

If there is a file called contact.txt in the flash filesystem then this app
allows it to be displayed three lines at a time using the pager.

This application also (optionally) loads an icon from the filesystem allowing
to be customized to match whether theme your verses are based around.
"""

import wasp

import io
import sys

from apps.pager import PagerApp


class ContactInfoApp(PagerApp):
    NAME = 'Contact'

    def __init__(self):
        # Throw an exception if there is no poetry for us to read...
        file = open('contact.txt')

        try:
            with open('contact.rle', 'rb') as f:
                self.ICON = f.read()
        except:
            # Leave the default app icon if none is present
            pass

        super().__init__(file.read())
        file.close()

    def foreground(self):
        super().foreground()
