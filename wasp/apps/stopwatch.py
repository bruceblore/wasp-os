# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson

"""Stopwatch
~~~~~~~~~~~~

Simple stop/start watch with support for split times.

.. figure:: res/StopclockApp.png
    :width: 179
"""
import wasp


class StopwatchApp():
    """Stopwatch application."""
    # Stopwatch requires too many pixels to fit into the launcher

    NAME = 'Stopclock'
    ICON = wasp.icons.app

    def __init__(self):
        self._timer = wasp.widgets.Stopwatch(120-36)
        self._reset()

    def foreground(self):
        """Activate the application."""
        wasp.system.bar.clock = True
        self._lastInteractionTime = wasp.watch.rtc.get_uptime_ms()
        self._draw()
        wasp.system.request_tick(97)
        wasp.system.request_event(wasp.EventMask.TOUCH |
                                  wasp.EventMask.BUTTON |
                                  wasp.EventMask.NEXT)

    def background(self):
        # Undo our power saving to keep other apps usable
        wasp.watch.backlight.set(wasp.system._brightness)
        wasp.watch.display.poweron()
        wasp.watch.display.mute(False)

    def sleep(self):
        return True

    def wake(self):
        self._lastInteractionTime = wasp.watch.rtc.get_uptime_ms()
        self._update()

    def swipe(self, event):
        """Handle NEXT events by augmenting the default processing by resetting
        the count if we are not currently timing something.

        No other swipe event is possible for this application.
        """
        wasp.watch.backlight.set(wasp.system._brightness)
        wasp.watch.display.poweron()
        wasp.watch.display.mute(False)
        if not self._started_at:
            self._reset()
        return True     # Request system default handling

    def press(self, button, state):
        if not state:
            return

        if self._timer.started:
            self._timer.stop()
        else:
            self._timer.start()

        wasp.watch.backlight.set(wasp.system._brightness)
        wasp.watch.display.poweron()
        wasp.watch.display.mute(False)

        self._lastInteractionTime = wasp.watch.rtc.get_uptime_ms()
        wasp.watch.vibrator.pulse(duty=50, ms=50)

    def touch(self, event):
        if self._timer.started:
            self._splits.insert(0, self._timer.count)
            del self._splits[4:]
            self._nsplits += 1
        else:
            self._reset()

        wasp.watch.backlight.set(wasp.system._brightness)
        wasp.watch.display.poweron()
        wasp.watch.display.mute(False)

        self._lastInteractionTime = wasp.watch.rtc.get_uptime_ms()
        wasp.watch.vibrator.pulse(duty=50, ms=200)
        self._update()
        self._draw_splits()

    def tick(self, ticks):
        if (wasp.watch.rtc.get_uptime_ms() - self._lastInteractionTime) >= 15000:
            wasp.watch.display.mute(True)
            wasp.watch.backlight.set(0)
            wasp.watch.display.poweroff()
        wasp.system.keep_awake()
        self._update()

    def _reset(self):
        self._timer.reset()
        self._splits = []
        self._nsplits = 0

    def _draw_splits(self):
        draw = wasp.watch.drawable
        splits = self._splits
        if 0 == len(splits):
            draw.fill(0, 0, 120, 240, 120)
            return
        y = 240 - 6 - (len(splits) * 24)

        draw.set_font(wasp.fonts.sans24)
        draw.set_color(wasp.system.theme('mid'))

        n = self._nsplits
        for i, s in enumerate(splits):
            centisecs = s
            secs = centisecs // 100
            centisecs %= 100
            minutes = secs // 60
            secs %= 60

            t = '# {}   {:02}:{:02}.{:02}'.format(n, minutes, secs, centisecs)
            n -= 1

            w = wasp.fonts.width(wasp.fonts.sans24, t)
            draw.string(t, 0, y + (i*24), 240)

    def _draw(self):
        """Draw the display from scratch."""
        draw = wasp.watch.drawable
        draw.fill()

        wasp.system.bar.draw()
        self._timer.draw()
        self._draw_splits()

    def _update(self):
        wasp.system.bar.update()
        self._timer.update()
