# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson

"""Sports timer
~~~~~~~~~~~~~~~

A combined stopwatch and step counter.

.. figure:: res/SportsApp.png
    :width: 179
"""
import wasp


class SportsApp():
    """Sports timer application."""
    NAME = 'Sports'

    # 2-bit RLE, 96x64, generated from res/sports_icon.png, 247 bytes
    ICON = (
        b'\x02'
        b'`@'
        b'6\xc6?\x1a\xc9?\x17\xca?\x17\xcb?\x15\xcc?\x15'
        b'\xcd?\x14\xcc?\x16\xcb?\x16\xcb?\x17\xc9?\x19\xc7'
        b'?\x1d\xc2?\x0c\xd0?\x10\xd3?\r\xd4?\x0c\xd6?'
        b'\x0b\xd7?\t\xd9?\x07\xda?\x07\xc7\x06\xce?\x05\xc8'
        b'\x06\xcf?\x04\xc7\x06\xd1?\x02\xc8\x06\xd1?\x01\xc8\x06'
        b'\xd3\x06\xc54\xc7\x07\xca\x02\xd24\xc7\x06\xcb\x03\xd14'
        b'\xc6\x07\xca\x04\xd15\xc4\x07\xcb\x05\xd07\xc1\x08\xcb\x06'
        b'\xcf?\x01\xca\x08\xcd?\x01\xca\t\xc7?\x07\xca?\x16'
        b'\xcb?\x16\xca?\x17\xcc?\x15\xcd?\x14\xce?\x13\xcf'
        b'?\x11\xd2?\x0f\xc7\x01\xcb?\x0e\xc7\x03\xca?\r\xc7'
        b'\x04\xca?\x0c\xc6\x06\xca?\x0b\xc6\x07\xca?\t\xc7\t'
        b'\xc8?\t\xc7\n\xc78\xd7\n\xc78\xd7\n\xc78\xd6'
        b'\x0b\xc78\xd6\x0b\xc78\xd6\x0b\xc78\xd5\x0c\xc78\xcd'
        b'\x14\xc7?\x1a\xc7?\x1a\xc7?\x1a\xc7?\x1a\xc7?\x1a'
        b'\xc7?\x1a\xc7?\x1a\xc7?\x1a\xc7?\x1a\xc7?\x1b\xc5'
        b'?\x1d\xc4#'
    )

    def __init__(self):
        self._timer = wasp.widgets.Stopwatch(120-36)
        self._reset()

    def foreground(self):
        """Activate the application."""
        wasp.system.bar.clock = True
        self._draw()
        wasp.system.request_tick(97)
        wasp.system.request_event(wasp.EventMask.TOUCH | wasp.EventMask.BUTTON)

    def background(self):
        if not self._timer.started:
            self._timer.reset()

    def sleep(self):
        return True

    def wake(self):
        self._update()

    def press(self, button, state):
        if not state:
            return

        steps = wasp.watch.accel.steps

        if self._timer.started:
            self._timer.stop()
        else:
            self._timer.start()
            self._last_steps = steps

    def touch(self, event):
        if not self._timer.started:
            self._reset()
            self._update()

    def tick(self, ticks):
        self._update()

    def _reset(self):
        self._timer.reset()
        self._steps = 0
        self._last_steps = 0

    def _draw(self):
        """Draw the display from scratch."""
        draw = wasp.watch.drawable
        draw.fill()

        wasp.system.bar.draw()
        self._timer.draw()

    def _update(self):
        wasp.system.bar.update()
        self._timer.update()

        if self._timer.started:
            steps = wasp.watch.accel.steps
            redraw = bool(steps - self._last_steps)
            self._steps += steps - self._last_steps
            self._last_steps = steps
        else:
            redraw = True

        if redraw:
            draw = wasp.watch.drawable
            draw.set_font(wasp.fonts.sans36)
            draw.set_color(draw.lighten(wasp.system.theme(
                'spot1'), wasp.system.theme('contrast')))
            draw.string(str(self._steps), 0, 170, 228, True)
