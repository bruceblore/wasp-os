# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Johannes Wache
"""Calculator
~~~~~~~~~~~~~

This is a simple calculator app that uses the build-in eval() function to
compute the solution.

.. figure:: res/CalcApp.png
    :width: 179
"""

import wasp

# 2-bit RLE, generated from res/calc.png, 413 bytes
calc = (
    b'\x02'
    b'`@'
    b'(@\x03P?\x0eV?\x08[?\x04_?\x01b'
    b'<f9h7j5l3n1p/r-t'
    b'+H\xd1D\xd1H*H\xd2B\xd2H)I\xd2B'
    b"\xd2I'J\xd2B\xd2I'J\xc8\xc2\xc8B\xd2J"
    b'%K\xc7\xc1\x02\xc1\xc7B\xd2J%K\xc7\xc1\x02\xc2'
    b'\xc6B\xd2K$K\xc5\xc2\xc1\x02\xc1\xc2\xc5B\xd2K'
    b'#L\xc4\xc1\x08\xc1\xc4B\xc5\x08\xc5L"L\xc4\xc1'
    b'\x08\xc2\xc3B\xc5\x08\xc1\xc1\xc3L"L\xc5\xc2\xc1\x02'
    b'\xc8B\xcf\xc1\xc2L!M\xc6\xc2\x02\xc8B\xd0\xc1\xc1'
    b'M M\xc7\xc1\x02\xc8B\xd1\xc1M M\xc8\xc2\xc8'
    b'B\xd2M M\xc9\xc1\xc8B\xd2M M\xd2B\xd2'
    b'M M\xd2B\xd2M N\xd0D\xd0N \x7f\x01'
    b' \x7f\x01 N\xd0D\xc1\xcfN M\xd2B\xc1\xd1'
    b'M M\xd2B\xd2AL M\xd2B\xd2AL '
    b'M\xd2B\xd2AL M\xc7\xc1\xcaB\xc5\xc8\xc5A'
    b'L M\xc5\xc1\x02\xc2\x02\xc1\xc5B\xc4\xc1\x08\xc1\xc4'
    b'AL!L\xc5\xc1\x06\xc1\xc5B\xc4\xc1\x08\xc1\xc1\xc3'
    b'AK"L\xc6\xc1\x04\xc1\xc6B\xc6\xca\xc2AK"'
    b'L\xc6\xc1\x04\xc1\xc6B\xc7\xca\xc1L#K\xc5\xc1\x06'
    b'\xc1\xc5B\xc4\xc1\x08\xc1\xc4JA$K\xc5\xc1\x02\xc2'
    b'\x02\xc1\xc5B\xc4\xc1\x08\xc1\xc4K%J\xc6\xc2\xc2\xc2'
    b"\xc6B\xc5\xc8\xc5J&J\xd2B\xc6\xccJ'I\xd2"
    b'B\xc7\xcbI(I\xd2B\xc8\xcaI)H\xd2B\xc9'
    b'\xc9H*H\xd1D\xc9\xc8H+t-r/p1'
    b'n3l5j7h9f<b?\x01^?\x05'
    b'XAA?\tV?\x0eP(')

fields_num = ('789M'
              '4560'
              '123.')
fields_op = ('+-CM'
             '*/AE'
             '^RSE')


class CalculatorApp():
    NAME = 'Calc'
    ICON = calc
    op = False

    def __init__(self):
        self.x = ""
        self.y = 0
        self.z = 0
        self.t = 0
        self._lift = False  # Whether or not to lift the stack the next time a number is pressed
        self._clear = False  # Whether or not to clear the next time a number is pressed

    def foreground(self):
        self._draw()
        self._update()
        wasp.system.request_event(wasp.EventMask.TOUCH)

    def _drop(self, value):
        self.z, self.y, self.x = self.t, self.z, str(value)
        self._lift = True

    def touch(self, event):
        if (event[2] < 48):
            if (event[1] > 200):  # undo button pressed
                if (self.x != ""):
                    self.x = self.x[:-1]
        else:
            x = event[1] // 63
            y = (event[2] // 63) - 1

            # Error handling for touching at the border
            if x > 3:
                x = 4
            if y > 2:
                y = 2
            button_pressed = fields_op[x + 4 *
                                       y] if self.op else fields_num[x + 4*y]

            try:
                # Stack management
                if (button_pressed == "R"):  # Rotate down
                    self.x, self.y, self.z, self.t = str(
                        self.y), self.z, self.t, float(self.x)
                    self._lift = False
                    self._clear = True
                    wasp.watch.vibrator.pulse()
                elif (button_pressed == "S"):  # Swap x and y
                    self.x, self.y = str(self.y), float(self.x)
                    self._lift = False
                    self._clear = True
                    wasp.watch.vibrator.pulse()
                elif (button_pressed == "C"):  # Clear x
                    self.x = ""
                    self._lift = False
                    self._clear = False
                    wasp.watch.vibrator.pulse()
                elif (button_pressed == "E"):  # Enter: lift stack
                    self.t, self.z, self.y = self.z, self.y, float(
                        self.x)
                    self._lift = False
                    self._clear = True
                    wasp.watch.vibrator.pulse()
                elif (button_pressed == "A"):  # Clear All
                    self.t, self.z, self.y, self.x = 0, 0, 0, '0'
                    self._lift = False
                    self._clear = False
                    wasp.watch.vibrator.pulse()

                # Operations
                elif (button_pressed == "+"):
                    self._drop(float(self.x) + self.y)
                    self._lift = True
                    self._clear = True
                    wasp.watch.vibrator.pulse()
                elif (button_pressed == "-"):
                    if self.x == "":
                        self.x = "-"
                        self._lift = False
                        self._clear = False
                        wasp.watch.vibrator.pulse()
                    else:
                        self._drop(self.y - float(self.x))
                        self._lift = True
                        self._clear = True
                        wasp.watch.vibrator.pulse()
                elif (button_pressed == "*"):
                    self._drop(float(self.x) * self.y)
                    self._lift = True
                    self._clear = True
                    wasp.watch.vibrator.pulse()
                elif (button_pressed == "/"):
                    self._drop(self.y / float(self.x))
                    self._lift = True
                    self._clear = True
                    wasp.watch.vibrator.pulse()
                elif (button_pressed == "^"):
                    self._drop(self.y ** float(self.x))
                    self._lift = True
                    self._clear = True
                    wasp.watch.vibrator.pulse()

                # Mode switch
                elif (button_pressed == "M"):
                    self.op = not self.op
                    self._draw()

                # Numbers
                else:
                    if self._lift:
                        self.t, self.z, self.y = self.z, self.y, float(
                            self.x)
                        self._lift = False
                    if self._clear:
                        self.x = ""
                        self._clear = False
                    self.x += button_pressed
            except:  # Aggressive vibrate if invalid number
                wasp.watch.vibrator.pulse(duty=50, ms=500)
        self._update()

    def _draw(self):
        draw = wasp.watch.drawable

        hi = wasp.system.theme('bright')
        lo = wasp.system.theme('mid')
        mid = draw.lighten(lo, 2)
        bg = draw.darken(wasp.system.theme(
            'ui'), wasp.system.theme('contrast'))
        bg2 = draw.darken(bg, 2)

        # Draw the background
        draw.fill(0, 0, 0, 239, 47)
        draw.fill(0, 236, 239, 3)
        draw.fill(bg, 0, 48, 239, 235-48)

        # Make grid:
        draw.set_color(lo)
        for i in range(3):
            # horizontal lines
            draw.line(x0=0, y0=i*63+47, x1=239, y1=i*63+47)
            # vertical lines
            draw.line(x0=i*60+60, y0=47, x1=i*60+60, y1=236)
        draw.line(x0=0, y0=47, x1=0, y1=236)
        draw.line(x0=239, y0=47, x1=239, y1=236)
        draw.line(x0=0, y0=236, x1=239, y1=236)

        # Draw button labels
        draw.set_color(hi, bg)
        for x in range(4):
            if x == 3:
                draw.set_color(mid, bg)
            for y in range(3):
                label = fields_op[x + 4*y] if self.op else fields_num[x + 4*y]
                if (x == 0):
                    draw.string(label, x*60+24, y*60+72)
                else:
                    draw.string(label, x*60+24, y*60+72)
        draw.set_color(hi)
        draw.string("<", 215, 10)

    def _update(self):
        output = self.x if len(
            self.x) < 12 else self.x[len(self.x)-12:]
        wasp.watch.drawable.string(output, 0, 14, width=200, right=True)

    # Prevent the watch from going back to the clock when the screen times out. This is because I might be using the calculator for something more than a one-off, in which case it is annoying to have to re-open it every time. However, I would still like to save power.
    def sleep(self):
        return True

    def wake(self):
        self._draw()
        self._update()
