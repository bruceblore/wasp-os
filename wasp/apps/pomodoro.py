# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson

import wasp
from micropython import const
from time import sleep
from math import floor

_WORK = const(0)            # Indicates that it is time to work
_SHORT_BREAK = const(1)     # Indicates that it is time for a short break
_LONG_BREAK = const(2)      # Indicates that it is time for a long break
_NOT_STARTED = const(-1)    # Indicates that the timer has not been started

_BUTTON_Y = const(200)  # Y coordinate of play or stop button


class PomodoroApp():
    """An app for implementing the Pomodoro technique to help you be productive while also not burning out completely.
    The Pomodoro technique involves repeated cycles of 25 minutes of work followed by a 5 minute break most of the
    time, or a 15 minute break every 4th time. The watch will:
    * Vibrate strongly for an extended time at the start of a work period
    * Vibrate twice at prefered notification level at the start of a short break
    * Vibrate three times at preferred notification level at the start of a long break
    """

    NAME = 'Pomo'   # Apparently, 'Pomodoro' is too long

    # 2-bit RLE, 96x64, generated from res/pomodoro_icon.png, 166 bytes
    ICON = (
        b'\x02'
        b'`@'
        b' @\x12`?\x02^?\x04\\?\x06Z?\x08X'
        b'?\nV?\x0cT?\x0eR?\x10P?\x12N?'
        b'\x14L?\x16J?\x18H?\x1aF?\x1cD?\x1e'
        b'B?\x1b\x80\xb4\x8a?\x14\x90?\x0e\x96?\n\x98?'
        b'\x07\x9c?\x04\x9e?\x02\xa0?\x00\xa2=\xa4;\xa69'
        b'\xa88\xa87\xaa5\xac4\xac4\xac3\xae2\xae2'
        b'\xae1\xb00\xb00\xb00\xb00\xb00\xb00\xb00'
        b'\xb00\xb00\xb01\xae2\xae2\xae3\xac4\xac4'
        b'\xac5\xaa7\xa88\xa89\xa6;\xa4=\xa2?\x00\xa0'
        b'?\x02\x9e?\x04\x9c?\x07\x98?\n\x96?\x0e\x90?'
        b'\x14\x8a+'
    )

    _currentState = _NOT_STARTED  # Tracks the current state that the user should be in
    # Counts how many short breaks have passed since starting timer or last long break
    _breakCount = 0
    _nextAlarm = 0                 # The time that the next state change takes place

    def foreground(self):
        """Activate the application."""
        wasp.system.request_event(wasp.EventMask.TOUCH |
                                  wasp.EventMask.SWIPE_LEFTRIGHT)
        wasp.system.request_tick(1000)
        # Disable Bluetooth for security
        self._draw()

    def changeState(self):
        now = wasp.watch.rtc.time()
        if self._currentState == _WORK:
            if self._breakCount < 3:
                self._nextAlarm = now + 300  # 300sec = 5min
                self._currentState = _SHORT_BREAK
                wasp.watch.vibrator.pulse(ms=wasp.system.notify_duration)
                sleep(0.45)
                wasp.watch.vibrator.pulse(ms=wasp.system.notify_duration)
            else:
                self._nextAlarm = now + 900  # 900sec = 15min
                self._currentState = _LONG_BREAK
                wasp.watch.vibrator.pulse(ms=wasp.system.notify_duration)
                sleep(0.45)
                wasp.watch.vibrator.pulse(ms=wasp.system.notify_duration)
                sleep(0.45)
                wasp.watch.vibrator.pulse(ms=wasp.system.notify_duration)
        else:
            if self._currentState == _SHORT_BREAK:
                self._breakCount += 1
            elif self._currentState == _LONG_BREAK:
                self._breakCount = 0
            self._nextAlarm = now + 1500  # 1500sec = 25min
            self._currentState = _WORK
            wasp.watch.vibrator.pulse(duty=0, ms=1000)

        wasp.system.set_alarm(self._nextAlarm, self.changeState)

    def swipe(self, event):
        """Notify the application of a touchscreen swipe event."""
        wasp.system.cancel_alarm(self._nextAlarm, self.changeState)
        self.changeState()
        self._update()

    def tick(self, ticks):
        self._update()

    def sleep(self):
        return True

    def touch(self, event):
        """Notify the application of a touchscreen touch event."""
        if event[2] >= _BUTTON_Y:
            if self._currentState == _NOT_STARTED:
                self.changeState()
            else:
                wasp.system.cancel_alarm(self._nextAlarm, self.changeState)
                self._currentState = _NOT_STARTED
            self._draw()

    def _draw(self):
        """Draw the display from scratch."""
        draw = wasp.watch.drawable
        draw.reset()
        draw.fill()

        draw.string(':', 110, 120-14, width=20)
        sbar = wasp.system.bar
        sbar.clock = True
        sbar.draw()

        if self._currentState == _NOT_STARTED:
            self._draw_play(104, _BUTTON_Y)
        else:
            self._draw_stop(104, _BUTTON_Y)

        self._update()

    def _update(self):
        draw = wasp.watch.drawable
        wasp.system.bar.update()

        if self._currentState == _NOT_STARTED:
            m = '25'
            s = '0'
        else:
            now = wasp.watch.rtc.time()
            s = self._nextAlarm - now
            if s < 0:
                s = 0
            m = str(floor(s // 60))
            s = str(floor(s) % 60)

        if len(m) < 2:
            m = '0' + m
        if len(s) < 2:
            s = '0' + s
        draw.set_font(wasp.fonts.sans28)
        draw.string(m, 0, 120-14, width=110, right=True)
        draw.string(s, 130, 120-14, width=60)

        if self._currentState == _NOT_STARTED:
            label = 'Not started'
        elif self._currentState == _WORK:
            label = 'Work {}/4'.format(self._breakCount + 1)
        elif self._currentState == _SHORT_BREAK:
            label = 'Short break {}/3'.format(self._breakCount + 1)
        else:
            label = 'Long break!'
        draw.set_font(wasp.fonts.sans18)
        draw.string(label, 0, 158, width=240)

    def _draw_play(self, x, y):
        draw = wasp.watch.drawable
        for i in range(0, 20):
            draw.fill(0xffff, x+i, y+i, 1, 40 - 2*i)

    def _draw_stop(self, x, y):
        wasp.watch.drawable.fill(0xffff, x, y, 40, 40)
