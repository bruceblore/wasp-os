# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson

import wasp
from micropython import const
# from cryptolib import aes
from hashlib import sha256 as sha


class AuthenticatorApp():
    """A TOTP authenticator
    WARNING: This app lacks plausible deniability and leaks information about how many entries you have. Additionally,
    pretty much anyone who gets within Bluetooth range can install malware on your watch (though admittedly that would
    have to be a pretty targeted attack as of now) and Wasp OS lacks memory protection, so it wouldn't be super hard
    to compromise this. However, it should be secure against someone simply stealing the watch and poking around, as
    the database is encrypted. You must consider your threat model to decide if this program is acceptable for you.

    NOTE: Because micropython doesn't ship the secrets module for cryptographically secure pseudorandom numbers, and
    the supported watches do not have hardware random number generators, it is insecure to generate the initial
    totp.txt file on the watch as it depends on a random salt. For this reason, the file totp.txt must be initially
    generated on another computer using tools/totp-writer.py and then uploaded to the watch.

    WARNING: It is possible to change the password on the watch itself, but due to the lack of suitable random number
    generation capability, the same salt and key will be re-used. If you suspect that either have become compromised
    somehow, you must backup your data and generate a new totp.txt file on another computer. Either may become
    compromised if generated on a computer with malware, or if someone both obtains a copy of totp.txt and knowledge
    of your password. There are two ways to make a backup:
        1. Make a backup on the watch, and then transfer the backup (totp.bak) to another computer
        2. Transfer totp.txt to another computer, and then make a backup on the simulator
    The first method will work if you do not have access to a computer which can run the simulator, but it is
    STRONGLY DISCOURAGED. The backup is in plaintext, so anyone who obtains access to it will have access to your TOTP
    secrets. The watch does not have full disk encryption, so a backup file will be unprotected from extraction through
    Bluetooth or physical means. If this is your only method of making a backup, it is recommended to not do it at all,
    but if you must, do it in a physically and electromagnetically safe place, delete it from the watch when done, and
    change all TOTP secrets as soon as possible.

    NOTE: Despite the fact that changing the password on the watch itself does not change the key and salt, it may
    still be OK to do so if you suspect that someone has learned the password but you have no reason to believe that
    they obtained the file. If someone you normally trust simply sees you enter your password, this might be such a
    situation. You must consider your threat model to decide if this is OK. It might also be beneficial to change the
    password on the watch itself immediately after generating totp.txt on another computer, even if you do not suspect
    a compromise, for extra protection against keyloggers.
    """

    NAME = 'TOTP'
    # 2-bit RLE, 96x64, generated from res/totp_icon.png, 357 bytes
    ICON = (
        b'\x02'
        b'`@'
        b'(@\xebP(%V%"\\" ` \x1fb'
        b'\x1f\x1df\x1d\x1ch\x1c\x1al\x1a\x19Q\x0cQ\x19\x18'
        b'O\x12O\x18\x17M\x18M\x17\x17L\x1aL\x17\x16K'
        b'\x1eK\x16\x15K K\x15\x15J"J\x15\x14J$'
        b'J\x14\x13J&J\x13\x13I(I\x13\x12J(J'
        b'\x12\x12I*I\x12\x12H,H\x12\x11I,I\x11'
        b'\x11I,I\x11\x11H.H\x11\x10I.I\x10\x10'
        b'I.I\x10\x10H0H\x10\x10H0H\x10\x10H'
        b'0H\x10\x10H0H\x10\x10H0H\x10\x10H0'
        b'H\x10\x10\\\xc3\x02\xc3\\\x10\x10Y\xc6\x02\xc6Y\x10'
        b'\x10W\xc8\x02\xc8W\x10\x10V\xc9\x02\xc9V\x10\x10U'
        b'\xca\x02\xcaU\x10\x10T\xcb\x02\xcbT\x10\x10S\xcc\x02'
        b'\xccS\x10\x10R\xcd\x02\xcdR\x10\x10R\xcd\x02\xcdR'
        b'\x10\x10Q\xce\x02\xceQ\x10\x10Q\xce\x02\xceQ\x10\x10'
        b'Q\xce\x02\xceQ\x10\x10\x90\xcf\x02\xcf\x90\x10\x10\x90\xcf'
        b'\x02\xcf\x90\x10\x10\x90\xcf\x02\xcf\x90\x10\x10\x90\xcf\x02\xcf'
        b'\x90\x10\x10\x90\xcf\x03\xce\x90\x10\x10\x90\xd0\x03\xcd\x90\x10'
        b'\x10\x90\xd1\x03\xcc\x90\x10\x10\x90\xd2\x03\xcb\x90\x10\x10Q'
        b'\xd2\x03\xc9Q\x10\x10Q\xd3\x03\xc8Q\x10\x10Q\xd4\x03'
        b'\xc7Q\x10\x10R\xd4\x01\xc7R\x10\x10R\xdcR\x10\x10'
        b'S\xdaS\x10\x10T\xd8T\x10\x10U\xd6U\x10\x10V'
        b'\xd4V\x10\x10W\xd2W\x10\x10Y\xceY\x10\x10\\\xc8'
        b'\\\x10'
    )
    _PASSWORD_PROMPT = const(1)  # Screen for entering the password
    _PASSWORD_CHANGE = const(2)  # Screen for changing the password
    _ENTRY_LIST = const(3)      # List of entries
    _ENTRY_VIEW = const(4)      # Viewing the current password of one entry
    # Editing the friendly name and secret of one entry
    _ENTRY_EDIT = const(5)

    # Stores which screen is currently being viewed
    _currentScreen = _PASSWORD_PROMPT
    _password = ''                      # Stores the password that was entered by the user
    _salt = b''                         # Stores the salt
    _saltKey = b''                      # Stores the key protecting the salt
    _key = b''                          # Stores the real encryption key
    # Track whether we are confirming a change, such as a password change or entry edit
    _confirming = False
    # Stores which line of the file the first entry on screen is on (for list) or current entry on screen (one entry)
    _position = 0
    _name = ''                          # Stores the name of the entry currently being viewed
    _secret = b''                       # Stores the TOTP secret
    _lastT9 = -1                        # Stores the last 0-9 key pressed

    # Open file for updating without truncation. This is harder to code for than just opening and closing when done. Note that this does not create the file if it doesn't exist, which will cause a crash. This is OK because micropython doesn't ship a cryptographically secure pseudorandom number generator and the watch lacks a true random number generator, so it is not secure to create the file on the watch anyways.
    _file = open('totp.txt', 'r+')

    def foreground(self):
        """Activate the application."""
        wasp.system.request_event(wasp.EventMask.TOUCH |
                                  wasp.EventMask.SWIPE_UPDOWN |
                                  wasp.EventMask.SWIPE_LEFTRIGHT)
        wasp.system.request_tick(1000)
        # TODO Disable Bluetooth for security
        self._currentScreen = self._PASSWORD_PROMPT
        self._password = ''
        self._key = ''
        self._draw()

    def background(self):
        """De-activate the application."""
        # Delete information that may be in memory
        self._currentScreen = self._PASSWORD_PROMPT
        self._password = ''
        self._key = ''
        # TODO Re-enable Bluetooth

    def swipe(self, event):
        """Notify the application of a touchscreen swipe event."""
        if self._currentScreen == self._PASSWORD_PROMPT:  # On password prompt screen, we don't have to do anything so a swipe exits the application
            wasp.system.navigate(event[0])
        # On password change screen, a swipe aborts the change
        elif self._currentScreen == self._PASSWORD_CHANGE:
            self._currentScreen = self._ENTRY_LIST
            self._draw()
        # On entry list screen, a swipe up/down scrolls and a swipe left/right exits
        elif self._currentScreen == self._ENTRY_LIST:
            if event[0] == wasp.EventType.UP:
                # TODO scroll up if possible
                print('Not yet implemented')
                self._update()
            elif event[0] == wasp.EventType.DOWN:
                # TODO scroll down if possible
                print('Not yet implemented')
                self._update()
            else:
                wasp.system.navigate(event[0])
        elif self._currentScreen == _ENTRY_VIEW:  # On entry view screen, a swipe left/right goes back to entry list and swipe up/down scrolls through entries
            if event[0] == wasp.EventType.UP:
                # TODO previous entry if possible
                print('Not yet implemented')
            elif event[0] == wasp.EventType.DOWN:
                # TODO next entry if possible
                print('Not yet implemented')
            else:
                self._currentScreen = self._ENTRY_LIST
            self._draw()  # Entry name is not normally updateable, so redrawing the screen, not just updating
        else:  # On entry edit screen, abort edit
            self._currentScreen = self._ENTRY_VIEW
            self._draw()

    def tick(self, ticks):
        self._update()

    def touch(self, event):
        """Notify the application of a touchscreen touch event."""
        draw = wasp.watch.drawable
        wasp.watch.drawable.string('({}, {})'.format(
            event[1], event[2]), 0, 108, width=240)

    def _draw(self):
        """Draw the display from scratch."""
        draw = wasp.watch.drawable
        draw.fill()
        draw.string(self.NAME, 0, 6, width=240)
        self._update()

    def _update(self):
        """Update the dynamic parts of the application display."""
        pass
